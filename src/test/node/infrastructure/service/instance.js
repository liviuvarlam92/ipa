//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global it */
/* global require */
/* global describe */

const assert = require('assert');
const Instance = require('../../../../main/node/infrastructure/service/instance');

describe('Instance', () => {

  const testObj = new Instance('0', 'http', 'localhost', 3030, '/abc');

  it('does not propagate authorization header', () => {
    const request = {
      query: {},
      headers: {
        'accept': 'application/json',
        'authorization': 'Api-Key abcd'
      },
      method: 'get'
    }
    const actual = testObj.buildRequestOptions(request);
    assert.ok(actual.headers['authorization'] == undefined, 'authorization header should not be propagated');
  });

  it('does not propagate host header', () => {
    const request = {
      query: {},
      headers: {
        'accept': 'application/json',
        'host': 'localhost'
      },
      method: 'get'
    }
    const actual = testObj.buildRequestOptions(request);
    assert.ok(actual.headers['host'] == undefined, 'host header should not be propagated');
  });
});