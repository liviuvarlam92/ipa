//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */
/* global require */

'use strict';

const cors = require('cors');
const apicache = require('apicache');
const passport = require('passport');
const objectAssignDeep = require('object-assign-deep');

const logger = require('./logger');

// const Component = require('dino-core').Component;
const RoutingConfigurer = require('dino-express').RoutingConfigurer;
/**
 * Define the application routing
 */
class ApplicationRouting extends RoutingConfigurer {

  constructor({ applicationContext, environment }) {
    super({ applicationContext: applicationContext, environment: environment });
    passport.serializeUser((user, done) => done(null, user));
    passport.deserializeUser((user, done) => done(null, user));
  }

  extend(server) {
    server.use(cors(this.environment.get('dino:server:cors') || {}));
    server.use(passport.initialize());
  }

  middlewares(api, components) {
    return [].concat(this.getPolicies(api.policies))
      .concat(this.getAuthenticator(api, components))
      .concat(this.getCache(api.cache));
  }

  requestHandler(api) {
    return this.applicationContext.resolve('serviceBuilder').build(api).invoke();
  }

  /**
   * Normalize the path to express notation
   * @param {String} path the API path
   * 
   * @private
   */
  normalizePath(path) {
    let answer = path;
    if (/.*{.*}.*/.test(path)) {
      answer = path.replace(/{/g, ':').replace(/}/, '');
      logger.info('path normalized to', answer);
    }
    return answer;
  }

  getRequestValidator(api) {
    let validator = this.applicationContext.resolve('requestValidator');
    validator.configure(api);
    return validator.validate.bind(validator);
  }

  /**
   * Get the policies defined for this route
   * @param {Array<String>} policies the policies defined for this route
   * @returns {Array<AbstractPolicy>} the policies
   * 
   * @private
   */
  getPolicies(policies = []) {
    return policies.reduce((answer, policy) => {
      let _policy = this.applicationContext.resolve('policyFactory')
        .getPolicy(policy.name);
      _policy.configure(policy.configuration);
      answer.push(_policy.middleware.bind(_policy));
      return answer;
    }, []);
  }

  /**
   * Get the authenticator for the authentication strategy requested.
   * @param {Any} api the API configuration definition
   * @param {any} components the components OpenAPI definition
   * @returns {Function} the authenticator for the requested strategy
   * 
   * @private
   */
  getAuthenticator(api, components) {
    let answer = (req, res, next) => next();
    const securitySchemes = api.security || [];
    answer = securitySchemes
      .reduce((acc, securitySchema) => {
        const security = Object.keys(securitySchema)[0];

        const localConfig = securitySchema[security][0];
        const globalConfig = components.securitySchemes[security];
        const config = objectAssignDeep({ api: api.operationId }, globalConfig, localConfig);

        logger.info('setting-up authenticator for strategy', config.strategy);
        let strategy = this.applicationContext.resolve('authenticationStrategyFactory')
          .getStrategy(config.strategy);
        strategy.activate(config);
        acc.push(strategy.authenticator());
        return acc;
      }, []);
    logger.debug('authentication strategy not defined using noop authenticator');
    return answer;
  }

  /**
   * Get the cache middleware configured as requested for the current route
   * @param {Aany} cache the cache configuration
   * @returns {Function} the cache middleware.
   * 
   * @private
   */
  getCache(cache) {
    let answer = (req, res, next) => next();
    if (cache) {
      return apicache.middleware(cache.ttl || '30 minutes', this.cacheOnlySuccessResponsesForGet());
    }
    return answer;
  }

  /**
   * Define a function that discriminate what responses should be cahced. In this case only responses 
   * with 200 response code from GET requests.
   * @returns {Function} the cache toggle function
   * 
   * @private
   */
  cacheOnlySuccessResponsesForGet() {
    return (req, res) => res.statusCode === 200 && req.method == 'GET';
  }

}

/**
 * @typedef ApplicationRouting
 */
module.exports = ApplicationRouting;