//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */
/* global require */

const logger = require('../logger');
const Service = require('./service');
const Component = require('dino-core').Component;

class ServiceBuilder extends Component {

  constructor({ applicationContext }) {
    super();

    this.applicationContext = applicationContext;
  }

  /**
   * Build the HTTP client able to invoke backend service for this API.
   * @param {Any} api the OpenAPI definition 
   */
  build(api) {
    let backend = api.backend;
    let discoveryService = this.applicationContext.resolve(backend.type);
    discoveryService.configure(backend);
    logger.info(`api ${api.operationId} configured to use backend service ${JSON.stringify(backend)}`)
    return new Service(backend.service, discoveryService);
  }

  /**
   * Handle proxy errors
   * @param {Any} err the error object
   * @param {Any} req the request from the client
   * @param {Any} res the response to be sent to the client
   * 
   * @private
   */
  handlerProxyError(err, req, res) {
    logger.error('unable to fulfill request', err);
    res.status(502).json({ message: 'Bad Gateway' });
  }
}

/**
 * Build the HTTP(s) client service that will route the requests to the backend service
 * for the APIs.
 * 
 * @typedef ClientBuilder
 * @public
 */
module.exports = ServiceBuilder;