//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */
/* global require */

const Docker = require('dockerode');

const logger = require('../../logger');
const Instance = require('../instance');
const AbstractDiscoveryService = require('../abstract.discovery.service');

class DockerDiscoveryService extends AbstractDiscoveryService {

  constructor() {
    super();
    this.docker = undefined;
  }

  /**
   * Discover all the reachable instances of the requested service.
   * @param {Any} backend the backend to discover, definition varies 
   * depending on the backend to support
   * @returns {Array<Service>} All discovered instances
   * 
   * @public
   * @implements AbstractDiscoveryService#discover(service)
   */
  doDiscover(backend) {
    let answer = [];
    try {
      return this.queryDockerForServiceInstances(backend);
    } catch (e) {
      logger.error('unable to access docker infrastructure', e);
    }
    return answer;
  }

  /**
   * Validate the configuration provided is as expected for the requested backend.
   * Specifically validate that **service** parameter is provided.
   * @param {Any} backend the backend to discover, definition varies 
   * depending on the backend to support 
   * @throws {Error} if the validation is not successful
   * 
   * @protected
   */
  validate(backend) {
    if (!backend.service) {
      throw new Error('service parameter must be provided for web backend definition');
    }
  }

  /**
   * Get a docker instance
   * @param {Any} backend the backend configuration
   * @returns {Dockerode} the dockerode docker instance
   */
  dockerInstance(backend) { // eslint-disable-line no-unused-vars
    if (!this.docker) {
      this.docker = new Docker();
    }
    return this.docker;
  }

  /**
   * Queries docker swarm for the instances associated with the requestd service, 
   * the IP address retrieved is always the first in the list.
   * @param {Any} backend the backend configuration
   */
  queryDockerForServiceInstances(backend) {
    
    const docker = this.dockerInstance(backend);
    const serviceName = backend.stack ? `${backend.stack}_${backend.service}` : backend.service;
    const protocol = backend.protocol || 'http';
    const port = backend.port || '80';
    const path = backend.path || '/';

    return docker.listServices({ name: serviceName })
      .then(res => {
        logger.debug('got response', res);
        return res.reduce((answer, instance) => {
          const serviceInstanceName = instance.Spec.Name;
          logger.debug(`inspecting ${serviceInstanceName} service to retrieve ${serviceName}`);
          if(instance.Spec.Name == serviceName) {
            answer.push(new Instance(instance.ID, protocol, instance.Endpoint.VirtualIPs[0].Addr.split('/')[0], port, path));
          }
          return answer;
        }, []);
      })
      .catch(e => {
        logger.error(`unable to list instances for service ${backend.service}`, e);
        return [];
      });
  }
}

/**
 * Discovery service for docker based infrastructure.
 * 
 * @public
 * @implements AbstractDiscoveryService
 * @typedef DockerDiscoveryService
 */
module.exports = DockerDiscoveryService;