//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */
/* global require */

const logger = require('../logger');

class Service {

  /**
   * Create a new service.
   * 
   * @param {String} name the service name
   * @param {AbstractDiscoveryService} discoveryService the discovery service implementation 
   * that will allow retrive instances for this service
   * 
   * @public
   */
  constructor(name, discoveryService) {
    this.name = name;
    this.discoveryService = discoveryService;

    this.totalRequests = 0;
    this.responses = {};
  }

  /**
   * Define an Express JS pluggable middleware that invokes an instance if this service
   * @param {Any} req the client request
   * @param {Any} res the client response
   * 
   * @public
   */
  invoke() {
    return (async (req, res) => {
      this.totalRequests += 1;
      try {
        (await this.nextInstance()).invoke(req)
          .then(response => {
            logger.debug('got response from upstream service');
            this.propagateResponse(res, response);
          })
          .catch(error => this.errorHandler(error, res));
      } catch (e) {
        logger.error('unable to invoke service', e);
        res.status(500).json({ message: 'Internal Server Error' });
      }
    }).bind(this);
  }

  /**
   * Retrieve the next instance that can be interrogated for this service.
   * 
   * @returns {Instance} the next instance of this service, the returned value is undefined 
   * if no instances are found.
   * 
   * @private
   */
  async nextInstance() {
    let answer = undefined;
    let instances = await this.discoveryService.discover();
    let availableInstances = instances.length;
    if (availableInstances > 0) {
      answer = instances[this.totalRequests % availableInstances];
    }
    return answer;
  }

  /**
   * Error handler for remote requests
   * @param {Any} error the error received from upstream
   * @param {Any} response the clinet response
   * 
   * @private
   */
  errorHandler(error, response) {
    logger.error(`unable to invoke backend service ${error}`);
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      // console.log(error.response.data);
      // console.log(error.response.status);
      // console.log(error.response.headers);
      this.propagateResponse(response, error.response);
    } else {
      //  if (error.request) {
      // The request was made but no response was received
      // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
      // http.ClientRequest in node.js
      logger.debug('unable to complete the requested transaction', error.request, error.message);
      response.status(502).json({ message: 'Bad Gateway' });
    }
  }

  /**
   * Copy the response received from the server to the response received from the client.
   * @param {Any} clientResponse the client response
   * @param {Any} serverResponse the server response
   * 
   * @private
   */
  propagateResponse(clientResponse, serverResponse) {
    try {
      // https://gitlab.com/codesketch/ipa/issues/5
      // identity the response type based on the response content-type and decide what to do with the response.
      let responseData = this.extractResponseData(serverResponse);
      clientResponse.status(serverResponse.status);
      Object.entries(serverResponse.headers || {}).forEach(entry => clientResponse.set(entry[0], entry[1]));
      clientResponse.send(responseData);
    } catch (e) {
      logger.error('unable to handle response', e);
      clientResponse.status(500).json({ message: 'Internal Server Error' });
    }
  }

  extractResponseData(res) {
    let answer = res.data;
    if (res.headers['content-type']) {
      const match = /.+\/(.+)/gi.exec(res.headers['content-type']);
      if (['javascript', 'json', 'plain', 'csv', 'xml', 'css', 'html',].some(st => match[1] == st)) {
        answer = (match[1] == 'json') ? answer : answer.toString('utf-8');
      }
    }
    return answer;
  }
}

/**
 * Define a backend service.
 * @public
 * @typedef Service
 */
module.exports = Service;