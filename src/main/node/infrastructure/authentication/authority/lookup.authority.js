//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */
/* global require */

'use strict';

const logger = require('../../logger');
const AbstractAuthority = require('../abstract.authority');
const ForbiddenException = require('../../exceptions/forbidden.exception');

class LookupAuthority extends AbstractAuthority {

  /**
   * no-op validation.
   * @param {Profile} profile the profile provided by the authorizations step
   * @param {Any} configuration the aonfiguration provided by the user for this authority.
   * @returns {Promise<Any>} a resolved promise
   * 
   * @implements AbstractIdentityValidator#validate(profile);
   * @public
   */
  validate(profile, configuration) { // eslint-disable-line no-unused-vars
    logger.debug(`authorize user using lookup authority for ${configuration.api}`);
    let answer = undefined;
    let db = configuration.authority.db || {};
    if ((db[profile.getId()] || []).includes(configuration.api)) {
      logger.debug(`user authorize for ${configuration.api}`);
      answer = Promise.resolve(profile);
    } else {
      logger.error(`user is not authorized to access ${configuration.api}`);
      answer = Promise.reject(ForbiddenException.create(`user not authorized to access ${configuration.api} API`));
    }
    return answer;
  }
}

/**
 * Authorize the current authenticated user based on a lookup.
 * @public
 * @typedef LookupAuthority
 */
module.exports = LookupAuthority;