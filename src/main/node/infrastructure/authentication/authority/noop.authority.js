//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */
/* global require */

'use strict';

const AbstractAuthority = require('../abstract.authority');

class NoopAuthority extends AbstractAuthority {

  /**
   * no-op validation.
   * @param {Profile} profile the profile provided by the authorizations step
   * @param {Any} configuration the aonfiguration provided by the user for this authority.
   * @returns {Promise<Any>} a resolved promise
   * 
   * @implements AbstractIdentityValidator#validate(profile);
   * @public
   */
  validate(profile, configuration) { // eslint-disable-line no-unused-vars
    return Promise.resolve(profile);
  }
}

/**
 * @public
 * @typedef NoopAuthority
 */
module.exports = NoopAuthority;