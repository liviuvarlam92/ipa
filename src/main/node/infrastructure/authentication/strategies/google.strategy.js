//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */
/* global require */

'use strict';

const passport = require('passport');
const GoogleTokenStrategy = require('passport-google-idtoken');

const Profile = require('../profile');
const AbstractStrategy = require('../abstract.strategy');

class GoogleStrategy extends AbstractStrategy {

  /**
   * @extends AbstractStrategy#doActivate()
   */
  doActivate(authority) {
    passport.use(this.generateStrategyName('google'), new GoogleTokenStrategy(this.getStrategyConfiguration('google-token'),
      function (profile, done) {
        // verify user is active with google id
        return this.authorize(authority, Profile.create(profile.id, 'google'), done);
      }
    ));
  }

  /**
   * @extends AbstractStrategy#authenticator();
   */
  authenticator() {
    return passport.authenticate(this.generateStrategyName('google'));
  }

  static scope() {
    return 'transient';
  }
}

/**
 * @typedef FacebookStrategy
 */
module.exports = GoogleStrategy;