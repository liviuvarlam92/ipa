//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */
/* global require */
/* global setInterval */

'use strict';

const logger = require('../logger');
const AbstractPolicy = require('./abstract.policy');

class QuotaPolicy extends AbstractPolicy {

  constructor() {
    super();
    this.buckets = {};
  }

  /**
   * Apply the quota policy. The quota is calculated based on **window** and **allow** configuration 
   * parameters and based on the IP address of the remote client.
   * 
   * @param {Any} req the client request
   * @returns true if the number of requests on the defined window is less than the allow configuration 
   * parameter for the specific client, false otherwise
   * 
   * @protected
   * @implements AbstractPolicy#apply(req)
   */
  apply(req, res) { // eslint-disable-line no-unused-vars
    let now = Date.now();
    let key = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    let bucket = this.buckets[key];
    if (!bucket) {
      bucket = { count: 0, expires: now + this.configuration.window };
      this.buckets[key] = bucket;
    }

    if (now > bucket.expires) {
      bucket.count = 0;
      bucket.expires = now + this.configuration.window;
    }
    bucket.count += 1;
    logger.debug('Bucket', bucket);
    return bucket.count <= this.configuration.allow;
  }

  /**
   * Allow to configure this policy. Accepted configuration parameters are:
   *  - **resetInterval**, the interval at which the quotas shold be reset expressed in ms
   *  - **window**, the time window the quota should be defined for expressed in ms
   *  - **allow**, the number of requests that are allowed inside the window interval
   * @param {Any} configuration the policy configuration
   * 
   * @public
   */
  configure(configuration) {
    this.configuration = configuration;
    this.cleanUpJob = setInterval(this.removeExpired.bind(this), configuration.resetInterval || 30000);
  }

  /**
   * Execute logic that will signal that the policy has denied the client request
   * @param {Any} res the client response
   * 
   * @protected
   * @implements AbstractPolicy#onDeny(res)
   */
  onDeny(res) {
    // res.setHeader('X-RateLimit-Limit', reply.allowed);
    // res.setHeader('X-RateLimit-Remaining', reply.allowed - reply.used);
    // res.setHeader('X-RateLimit-Reset', (reply.expiryTime / 1000) >> 0);
    res.status(429).json({ message: 'Quota Exceeded' });
  }

  /**
   * Remove expired buckets
   * 
   * @private
   */
  removeExpired() {
    logger.debug('remove expires buckets');
    var now = Date.now();
    for (var b in Object.keys(this.buckets)) {
      if (now > b.expires) {
        delete this.buckets[b];
      }
    }
  }

  static scope() {
    return 'transient';
  }
}

/**
 * @typedef QuotaPolicy
 * @public
 */
module.exports = QuotaPolicy;